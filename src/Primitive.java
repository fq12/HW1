
public class Primitive {
	public static void main(String[] args) {
		double a=2;
		int b=3;
		boolean c= false;
		char d='d';
		byte e=1;
		float f=0.1f;
		long g=1000l;
		short h=3;
		
		Integer y=b;
		Boolean boo=c;
		Character ch=d;
		System.out.println("Integer and int "+y+" "+b);
		System.out.println("Boolean and boolean "+boo+" "+c);
		System.out.println("Character and char "+ch+" "+d);
		System.out.println("Others"+a+b+e+f+g+h);
	}
}
