import desserts.Biscuit;

public class Pointer {
	public static void main(String[] args) {
		passByValue();
		passReferencesByValue();
		System.out.println("--------------");
		System.out.println("--------------");
		useEquals();
	}
	
	private static void passByValue() {
		Biscuit d = new Biscuit("M","R");
		d.yummy();
		d.like();
	
		if (d.getName().equals("M")) { //true
			System.out.println( "Equal name" );
	
		} else if (d.getName().equals("Fifi")) {
			System.out.println( "Equal reference" );
		}
	}
	
	private static void useEquals() {
		String str1 = "M";
		String str2 = "M";
		if(str1==str2){
			System.out.println("comparing objects.");
		}
		if(str1.equals(str2)){
			System.out.println("Use .equals()");
		}
		
	}
	
	private static void passReferencesByValue() {
		Biscuit d = new Biscuit("M","R");
		bar(d);
		if (d.getName().equals("Max")) { //true
			System.out.println( "Equal name" );

		} else if (d.getName().equals("Fifi")) {
			System.out.println( "Equal reference" );
		}
		
	}

	private static void bar(Biscuit dog) {
		dog.rename("Fifi");
	}


	
}
