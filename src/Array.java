import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Array{
	public static void main(String[] args) {
		arraytest();
		maptest();
		listtest();
		settest();
	}
	
	public static void arraytest(){
		int [] array = new int [5];
		int i=0;
		for(i=0;i<5;i++){
			array[i]=i*i;
			System.out.println(array[i]);
		}
	}
	
	public static void maptest(){
		Map<String,String> map = new TreeMap<String,String>();
		map.put("Today", "Sunny");
		map.put("Tomorrow", "Rainy");
		map.put("Yesterday", "Cloudy");
		for(String i:map.keySet()){
			System.out.println("Weather : "+i+" "+map.get(i));
		}	
	}
	
	public static void listtest(){
		List<Integer> list = new ArrayList<>();
		int i;
		for(i=0;i<4;i++){
			list.add(i+1);
			System.out.println(list.get(i));
		}
	}
	
	public static void settest(){
		Set<String> set = new HashSet<>();
		set.add("A");
		set.add("B");
		set.add("C");
		set.add("D");
		for(String i:set){
			System.out.print(i+" ");
		}
	}
	
	
	
	
	

}
