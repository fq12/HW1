
public class Exception {
	private static void over(int x) throws ArithmeticException{
		if(x>99){
			throw new ArithmeticException("too much");
		}
		else{
			System.out.println("normal");
		}
	}
	
	private static void trytry (int x){
		try{
			System.out.println(5/x);
		}
		catch(ArithmeticException e){
			System.out.println("We can't divide by zero");
			x+=1;
			System.out.println(5/x);
		}
	}
	
	public static void main(String[] args) {
		int overflow = 100;
		int safe = 0;
		
		try{
			over(overflow);
		}
		catch(ArithmeticException e){
			System.err.println(e.getMessage());
		}
		
		try{
			over(safe);
		}
		catch(ArithmeticException e){
			System.err.println(e.getMessage());
			System.out.println("Something happened");
		}
		
		trytry(safe);
		trytry(overflow);
		
	}

}
