package desserts;

public class Biscuit extends Dessert implements Like{
	public Biscuit(String name,String color){
		super(name,color);
		System.out.println("It is a Biscuit");
	}
	
	@Override
	public void yummy(){
		System.out.println("Kind of");
	}
	
	@Override
	public void like(){
		System.out.println("Maybe not");
	}

   
}
