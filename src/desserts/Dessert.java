package desserts;

public abstract class Dessert {
	private String name,color;
	
	public Dessert(String name, String color){
		this.name=name;
		this.color=color;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public void rename(String name){
		this.name=name;
	}
	
	public void recolor(String color){
		this.color=name;
	}
	
	public abstract void yummy();

}
