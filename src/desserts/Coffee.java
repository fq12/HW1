package desserts;

public class Coffee extends Dessert{
	
	public Coffee(String name,String color) {
		super(name,color);
		System.out.println("It is a Coffee");
	}
    
	@Override
	public void yummy(){
		System.out.println("Of coures");
	}
	
}
