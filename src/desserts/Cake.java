package desserts;

public class Cake extends Dessert implements Like {
	public Cake(String name,String color){
		super(name,color);
		System.out.println("It is a Cake");
	}
	
	@Override
	public void yummy(){
		System.out.println("Mostly");
	}
	
	@Override
	public void like(){
		System.out.println("It depends");
	}

}
