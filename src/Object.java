import desserts.Biscuit;
import desserts.Cake;
import desserts.Coffee;
import desserts.Dessert;

public class Object {
	public static void main(String[] args) {
		Dessert a = new Cake("a","red");
		Dessert b = new Coffee("b","brown");
		Dessert c = new Biscuit("c","white");
		Dessert d = new Cake("d","blue");
		Dessert[] e = new Dessert[4];
		e[0]=a;e[1]=b;e[2]=c;e[3]=d;
		int i;
		for(i=0;i<e.length;i++){
			e[i].yummy();
			System.out.println("color "+e[i].getColor());
			System.out.println("name "+e[i].getName());
		}
		
	}

}
