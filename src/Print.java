
public class Print {
	private static void test1(int x){
		String s = "test for printing";
		System.out.println(x);
		System.out.println(s+" "+x);
		System.out.println("-------------------");	
	}
	
	private static void test2(int x){
		int y=x*x;
		System.out.println("x*x = "+y);
		System.out.println("-------------------");
	}
	
	private static void test3(){
		System.out.println();
		System.out.print("\n");
		System.out.println("-------------------");
	}
	
	public static void main(String[] args) {
		int x=0;
		test1(x);
		test2(x);
		test3();
	}

}
